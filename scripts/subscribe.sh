#!/bin/bash

# 1. Un-comment the following 3 lines to install latest mosquitto client, for once only

#sudo apt-add-repository ppa:mosquitto-dev/mosquitto-ppa
#sudo apt-get update
#sudo apt-get install mosquitto mosquitto-clients -y

# 2. Invoke UGS API: https://fsp.borqs.io/ugs/api/iot/connector?ugs_token={ugs_token}
#    with body like e.g.: {"device": {"id": "5ca2d5d9750a9206a97ff0b7", "type": "watch"}}
#    Response will be like:
#{
#    "code": 200,
#    "message": "AWS IOT Certificate created successfully",
#    "iotConnection": {
#        "endpoint": "a3ls9848c5dvx0.iot.ap-southeast-1.amazonaws.com",
#        "certificate": {
#            "clientCertificate": "-----BEGIN CERTIFICATE-----\nBASE64\n-----END CERTIFICATE-----\n",
#            "privateKey": "-----BEGIN RSA PRIVATE KEY-----\nBASE64\n-----END RSA PRIVATE KEY-----\n",
#            "rootCa": "-----BEGIN CERTIFICATE-----\nBASE64\n-----END CERTIFICATE-----\n"
#        },
#        "clientId": "5c4aa1c3e4b0dc290b515eb3_fsp_watch_5ca2d5d9750a9206a97ff0b7"
#    }
#}

# 3. Save the 3 text parts of certificates into separate files:
#    clientCertificate => client-certificate.pem.crt
#    privateKey => private.pem.key
#    rootCa => RootCA.pem


# 4. Use the endpoint and clientId from the response in the following command:
#    Note the topic should be like: fsp/group/{your_group_id}/#
#    or the subscription will fail

mosquitto_sub -h a3ls9848c5dvx0.iot.ap-southeast-1.amazonaws.com -p 8883 -t fsp/group/5c4aa256e4b0dc290b515eb4/# -i 5c4aa1c3e4b0dc290b515eb3_fsp_watch_5ca2d5d9750a9206a97ff0b7 -d -v --cafile RootCA.pem --cert client-certificate.pem.crt --key private.pem.key
