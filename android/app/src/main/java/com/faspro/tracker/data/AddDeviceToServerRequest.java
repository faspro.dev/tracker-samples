/*
 * Copyright (c) 2019 FASPRO SYSTEMS CO., LTD. All rights reserved.
 *
 * This work is licensed under the terms of the MIT license.
 * For a copy, see <https://opensource.org/licenses/MIT>.
 */

package com.faspro.tracker.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddDeviceToServerRequest {
    @Expose
    @SerializedName("mac")
    private String mac;

    @Expose
    @SerializedName("type")
    private String type;

    @Expose
    @SerializedName("name")
    private String name;

    @Expose
    @SerializedName("phone")
    private String phone;

    @Expose
    @SerializedName("photo_base64")
    private String photo;

    @Expose
    @SerializedName("dob")
    private String dob;

    @Expose
    @SerializedName("gender")
    private String gender;

    @Expose
    @SerializedName("height")
    private int height;

    @Expose
    @SerializedName("weight")
    private int weight;

    public AddDeviceToServerRequest(String mac, String type, String name, String phone, String photo, String dob, String gender, int height, int weight) {
        this.mac = mac;
        this.type = type;
        this.name = name;
        this.phone = phone;
        this.photo = photo;
        this.dob = dob;
        this.gender = gender;
        this.height = height;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public String getMAC() {
        return mac;
    }
}
