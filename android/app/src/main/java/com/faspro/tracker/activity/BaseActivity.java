/*
 * Copyright (c) 2019 FASPRO SYSTEMS CO., LTD. All rights reserved.
 *
 * This work is licensed under the terms of the MIT license.
 * For a copy, see <https://opensource.org/licenses/MIT>.
 */

package com.faspro.tracker.activity;

import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;

import com.faspro.tracker.data.LoginSession;
import com.faspro.tracker.data.SavedDevice;
import com.faspro.tracker.data.SavedGroup;

public abstract class BaseActivity extends AppCompatActivity {

    protected static final String BASE_URL = "https://fsp.borqs.io/";
    private static final String PREF_UGS = "UGS_SHARED_PREF";
    private static final String KEY_UGS_TOKEN = "ugsToken";
    private static final String KEY_USER_ID = "userId";
    private static final String PREF_DEVICE = "DEVICE_SHARED_PREF";
    private static final String _ID = "_ID";
    private static final String KEY_DEVICE_ID = "deviceId";
    private static final String KEY_DEVICE_MAC = "deviceMAC";
    private static final String PREF_GROUP = "PREF_GROUP";
    private static final String KEY_GROUP_ID = "groupId";


    protected void saveLoginSession(String ugsToken, String userId) {
        SharedPreferences pref = getSharedPreferences(PREF_UGS, MODE_PRIVATE);
        pref.edit().putString(KEY_UGS_TOKEN, ugsToken).putString(KEY_USER_ID, userId).apply();
    }

    protected void saveDeivce(String _id, String deviceMAC, String deviceId) {
        SharedPreferences pref = getSharedPreferences(PREF_DEVICE, MODE_PRIVATE);
        pref.edit().putString(_ID, _id).putString(KEY_DEVICE_MAC, deviceMAC).putString(KEY_DEVICE_ID, deviceId).apply();
    }

    protected void saveGroup(String groupid) {
        SharedPreferences pref = getSharedPreferences(PREF_GROUP, MODE_PRIVATE);
        pref.edit().putString(KEY_GROUP_ID, groupid).apply();
    }

    protected SavedGroup getSavedGroup() {
        SharedPreferences pref = getSharedPreferences(PREF_GROUP, MODE_PRIVATE);
        return new SavedGroup(pref.getString(KEY_GROUP_ID, null));
    }

    protected LoginSession getSavedSession() {
        SharedPreferences pref = getSharedPreferences(PREF_UGS, MODE_PRIVATE);
        return new LoginSession(pref.getString(KEY_UGS_TOKEN, null), pref.getString(KEY_USER_ID, null));
    }

    protected SavedDevice getSavedDevice() {
        SharedPreferences pref = getSharedPreferences(PREF_DEVICE, MODE_PRIVATE);
        return new SavedDevice(pref.getString(_ID, null), pref.getString(KEY_DEVICE_MAC, null), pref.getString(KEY_DEVICE_ID, null));
    }

    protected void clearSavedSession() {
        SharedPreferences pref = getSharedPreferences(PREF_UGS, MODE_PRIVATE);
        pref.edit().clear().apply();
    }

    protected void clearSavedDevice() {
        SharedPreferences pref = getSharedPreferences(PREF_DEVICE, MODE_PRIVATE);
        pref.edit().clear().apply();
    }

}
