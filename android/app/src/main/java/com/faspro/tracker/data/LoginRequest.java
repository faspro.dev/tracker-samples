/*
 * Copyright (c) 2019 FASPRO SYSTEMS CO., LTD. All rights reserved.
 *
 * This work is licensed under the terms of the MIT license.
 * For a copy, see <https://opensource.org/licenses/MIT>.
 */

package com.faspro.tracker.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginRequest {
    @Expose
    @SerializedName("email")
    private String email;
    @Expose
    @SerializedName("type")
    private String type;
    @Expose
    @SerializedName("password")
    private String password;

    public LoginRequest(String email, String type, String password) {
        this.email = email;
        this.type = type;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getType() {
        return type;
    }

    public String getPassword() {
        return password;
    }
}
