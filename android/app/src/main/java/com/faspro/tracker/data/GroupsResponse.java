/*
 * Copyright (c) 2019 FASPRO SYSTEMS CO., LTD. All rights reserved.
 *
 * This work is licensed under the terms of the MIT license.
 * For a copy, see <https://opensource.org/licenses/MIT>.
 */

package com.faspro.tracker.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GroupsResponse {
    @SerializedName("groups")
    @Expose
    private List<GroupResponse> group;

    public List<GroupResponse> getGroup() {
        return group;
    }

    public class GroupResponse {
        @SerializedName("_id")
        @Expose
        private String id;

        public String getId() {
            return id;
        }
    }
}
