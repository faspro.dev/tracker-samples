/*
 * Copyright (c) 2019 FASPRO SYSTEMS CO., LTD. All rights reserved.
 *
 * This work is licensed under the terms of the MIT license.
 * For a copy, see <https://opensource.org/licenses/MIT>.
 */

package com.faspro.tracker.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {

    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("user")
    @Expose
    private LoginResponseUser user;

    @SerializedName("ugs_token_expiry")
    @Expose
    private String ugsTokenExpiry;

    @SerializedName("ugs_token")
    @Expose
    private String ugsToken;

    public LoginResponse(String code, String message, LoginResponseUser user, String ugsTokenExpiry, String ugsToken) {
        this.code = code;
        this.message = message;
        this.user = user;
        this.ugsTokenExpiry = ugsTokenExpiry;
        this.ugsToken = ugsToken;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LoginResponseUser getUser() {
        return user;
    }

    public void setUser(LoginResponseUser user) {
        this.user = user;
    }

    public String getUgsTokenExpiry() {
        return ugsTokenExpiry;
    }

    public void setUgsTokenExpiry(String ugsTokenExpiry) {
        this.ugsTokenExpiry = ugsTokenExpiry;
    }

    public String getUgsToken() {
        return ugsToken;
    }

    public void setUgsToken(String ugsToken) {
        this.ugsToken = ugsToken;
    }

    @Override
    public String toString() {
        return "LoginResponse{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", user='" + user + '\'' +
                ", ugsTokenExpiry='" + ugsTokenExpiry + '\'' +
                ", ugsToken='" + ugsToken + '\'' +
                '}';
    }
}
