/*
 * Copyright (c) 2019 FASPRO SYSTEMS CO., LTD. All rights reserved.
 *
 * This work is licensed under the terms of the MIT license.
 * For a copy, see <https://opensource.org/licenses/MIT>.
 */

package com.faspro.tracker.mqtt;

import android.app.Activity;
import android.util.Log;

import com.amazonaws.mobileconnectors.iot.AWSIotKeystoreHelper;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttClientStatusCallback;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttManager;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttNewMessageCallback;
import com.amazonaws.mobileconnectors.iot.AWSIotMqttQos;
import com.faspro.tracker.data.IotConnection;
import com.faspro.tracker.data.IotConnectionCertificate;

import java.io.File;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;

public class ConnectionManager {

    private static final String TAG = ConnectionManager.class.getSimpleName();
    private static final String KEYSTORE_ALIAS = "faspro";
    private static final String KEYSTORE_NAME = "faspro";
    private static final String KEYSTORE_PASSWORD = AWSIotKeystoreHelper.AWS_IOT_INTERNAL_KEYSTORE_PASSWORD;

    private String keyStorePath = null;
    private AWSIotMqttManager mqttManager = null;
    private boolean isConnected = false;
    private List<ConnectionListener> listeners = new ArrayList<>();

    public ConnectionManager(Activity activity, IotConnection iotConnection) {
        try {
            keyStorePath = activity.getApplicationContext().getDataDir().getPath();
            savePrivateKeyAndCerts(iotConnection.getCertificate());
            mqttManager = new AWSIotMqttManager(iotConnection.getClientId(), iotConnection.getEndpoint());
            if (activity instanceof ConnectionListener) {
                listeners.add((ConnectionListener) activity);
            }
            connect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void connect() {
        mqttManager.connect(getKeyStore(), new AWSIotMqttClientStatusCallback() {
                    @Override
                    public void onStatusChanged(AWSIotMqttClientStatus status, Throwable throwable) {
                        if (status == AWSIotMqttClientStatus.Connected) {
                            isConnected = true;
                            for (ConnectionListener listener : listeners) {
                                listener.onConnected();
                            }
                        } else {
                            isConnected = false;
                            if (status == AWSIotMqttClientStatus.ConnectionLost) {
                                for (ConnectionListener listener : listeners) {
                                    listener.onDisconnected(throwable);
                                }
                            } else if (status == AWSIotMqttClientStatus.Connecting) {
                                for (ConnectionListener listener : listeners) {
                                    listener.onConnecting(throwable);
                                }
                            } else if (status == AWSIotMqttClientStatus.Reconnecting) {
                                for (ConnectionListener listener : listeners) {
                                    listener.onReconnecting(throwable);
                                }
                            }
                        }
                    }
                }
        );
    }

    /**
     * Disconnect gracefully. Listener should wait for disconnected callback
     */
    public void disconnect() {
        if (mqttManager != null) {
            mqttManager.disconnect();
        }
    }

    /**
     * Force disconnect
     */
    public void close() {
        if (isConnected) {
            mqttManager.disconnect();
        }
        listeners.clear();
    }

    public void addListener(ConnectionListener listener) {
        listeners.add(listener);
    }

    public void subscribe(String topic) {
        try {
            if (isConnected) {
                Log.d(TAG, "to subscribe to " + topic);
                mqttManager.subscribeToTopic(topic, AWSIotMqttQos.QOS0, new AWSIotMqttNewMessageCallback() {
                    @Override
                    public void onMessageArrived(String topic, byte[] data) {
                        System.out.println("New message arrived");
                        System.out.println(new String(data));
                        Log.d(TAG, "subscribe result: " + data.toString());
                        for (ConnectionListener listener : listeners) {
                            listener.onMessageArrived(topic, data);
                        }
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void publish(String message, String topic) {
        try {
            if (isConnected) {
                mqttManager.publishString(message, topic, AWSIotMqttQos.QOS0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void savePrivateKeyAndCerts(IotConnectionCertificate cert) {
        String certPem = cert.getClientCertificate();
        String keyPem = cert.getPrivateKey();
        File file = new File(keyStorePath);
        if (file.exists())
            AWSIotKeystoreHelper.deleteKeystoreAlias(KEYSTORE_ALIAS, keyStorePath, KEYSTORE_NAME, KEYSTORE_PASSWORD);
        AWSIotKeystoreHelper.saveCertificateAndPrivateKey(KEYSTORE_ALIAS, certPem, keyPem, keyStorePath, KEYSTORE_NAME, KEYSTORE_PASSWORD);
    }

    private KeyStore getKeyStore() {
        return AWSIotKeystoreHelper.getIotKeystore(KEYSTORE_ALIAS, keyStorePath, KEYSTORE_NAME, KEYSTORE_PASSWORD);
    }
}
