/*
 * Copyright (c) 2019 FASPRO SYSTEMS CO., LTD. All rights reserved.
 *
 * This work is licensed under the terms of the MIT license.
 * For a copy, see <https://opensource.org/licenses/MIT>.
 */

package com.faspro.tracker.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IotConnection {
    @Expose
    @SerializedName("endpoint")
    private String endpoint;

    @Expose
    @SerializedName("certificate")
    private IotConnectionCertificate certificate;

    @Expose
    @SerializedName("clientId")
    private String clientId;

    public IotConnection(String endpoint, IotConnectionCertificate certificate, String clientId) {
        this.endpoint = endpoint;
        this.certificate = certificate;
        this.clientId = clientId;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public IotConnectionCertificate getCertificate() {
        return certificate;
    }

    public void setCertificate(IotConnectionCertificate certificate) {
        this.certificate = certificate;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Override
    public String toString() {
        return "IotConnection{" +
                "endpoint='" + endpoint + '\'' +
                ", certificate=" + certificate +
                ", clientId='" + clientId + '\'' +
                '}';
    }
}
