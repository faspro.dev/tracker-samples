/*
 * Copyright (c) 2019 FASPRO SYSTEMS CO., LTD. All rights reserved.
 *
 * This work is licensed under the terms of the MIT license.
 * For a copy, see <https://opensource.org/licenses/MIT>.
 */

package com.faspro.tracker.data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateDeviceSettingRequestDesired {
    @Expose
    @SerializedName("locPubInt")
    private int locPubInt;

    @Expose
    @SerializedName("locPubStart")
    private String locPubStart = "00:00";

    @Expose
    @SerializedName("locPubEnd")
    private String locPubEnd = "23:59";

    public UpdateDeviceSettingRequestDesired(int locPubInt) {
        this.locPubInt = locPubInt;
    }


}
