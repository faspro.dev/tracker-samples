/*
 * Copyright (c) 2019 FASPRO SYSTEMS CO., LTD. All rights reserved.
 *
 * This work is licensed under the terms of the MIT license.
 * For a copy, see <https://opensource.org/licenses/MIT>.
 */

package com.faspro.tracker.data;

public class SavedGroup {
    public String groupId;

    public SavedGroup(String groupId) {
        this.groupId = groupId;
    }
}
