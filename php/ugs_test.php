<?php

echo "UGS API Test\n";
$BASE_URI = "https://fsp.borqs.io";

echo "1. Request Reg Token\n";
$data = array("email" => "relaywatson@gmail.com");
$result = invoke("POST", "$BASE_URI/ugs/api/user/regtoken", json_encode($data));
echo "$result\n";

// Received Reg Token by e-mail

echo "2. Verify Reg Token\n";
$data = array(
	"email" => "relaywatson@gmail.com",
	"type" => "registration",
	"token" => "C373R"
);
$result = invoke("POST", "$BASE_URI/accounts/api/users/tokens/verify", json_encode($data));
echo "$result\n";

echo "3. Register New Account\n";
$data = array(
	"email" => "relaywatson@gmail.com",
	"name" => "Watson",
	"regToken" => "C373R",
	"phone" => "+886988000256",
	"type" => "supervisor",
	"dob" => "1988-8-8",
	"password" => "88888888"
);
$result = invoke("POST", "$BASE_URI/ugs/api/user/register", json_encode($data));
echo "$result\n";

echo "4. User Login\n";
$data = array(
	"email" => "relaywatson@gmail.com",
	"type" => "supervisor",
	"password" => "88888888"
);
$result = invoke("POST", "$BASE_URI/ugs/api/user/login", json_encode($data));
$json = json_decode($result, true);
$ugs_token = null;
if ($json['code'] == 200) {
	$ugs_token = $json['ugs_token'];
	echo "Success with ugs_token=$ugs_token\n";
} else {
	die("Failed: $result\n");
}

echo "5. Add Wearable\n";
$data = array(
	"mac" => "112233445566",
	"type" => "watch",
	"name" => "faspro"
);
$result = invoke("POST", "$BASE_URI/ugs/api/user/addwearable?ugs_token=$ugs_token", json_encode($data));
$user_id = null;
$json = json_decode($result, true);
if (isset($json['user'])) {
	$user_id = $json['user']['_id'];
	echo "Success with User ID: $user_id\n";
} else {
	echo "Failed: $result\n";
}

echo "6. Create Group\n";
$data = array(
	"title" => "FirstGroup",
	"photo_base64" => "Qk08AAAAAAAAADYAAAAoAAAAAQAAAAEAAAABABgAAAAAAAYAAAA3IQAANyEAAAAAAAAAAAAA////AAAA"
);
$result = invoke("POST", "$BASE_URI/ugs/api/group?ugs_token=$ugs_token", json_encode($data));
$json = json_decode($result, true);
if ($json['code'] == 200) {
	echo "Success $result\n";
} else {
	echo "Failed: $result\n";
}

echo "7. Get Groups\n";
$result = invoke("GET", "$BASE_URI/ugs/api/group?ugs_token=$ugs_token", null);
$json = json_decode($result, true);
$first_group_id = null;
if ($json['code'] == 200) {
	$groups = $json['groups'];
	if (count($groups) > 0) {
		$first_group_id = $groups[0]['_id'];
		echo "First Group ID: $first_group_id\n";
	}
} else {
	echo "Failed: $result\n";
}

if ($user_id && $first_group_id) {
	echo "8. Add User to Group\n";
	$data = array("_id" => $user_id);
	$result = invoke("POST", "$BASE_URI/ugs/api/group/$first_group_id/user?ugs_token=$ugs_token", json_encode($data));
	$json = json_decode($result, true);
	if ($json['code'] == 200) {
		echo "Success: $result\n";
	} else {
		echo "Failed: $result\n";
	}
}

function invoke($method, $url, $data) {
   $curl = curl_init();
   switch ($method) {
      case "POST":
         curl_setopt($curl, CURLOPT_POST, 1);
         if ($data) curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
         break;
      case "PUT":
         curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
         if ($data) curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
         break;
      default:
         if ($data) $url = sprintf("%s?%s", $url, http_build_query($data));
   }
   // OPTIONS:
   curl_setopt($curl, CURLOPT_URL, $url);
   curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'Content-Type: application/json',
   ));
   curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
   // EXECUTE:
   $result = curl_exec($curl);
   if (!$result) {die("Connection Failure");}
   curl_close($curl);
   return $result;
}

?>
